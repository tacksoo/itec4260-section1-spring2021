package org.example;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a physical game store like Gamestop, ToysRUs, etc
 */
public class Store {

    private String inventoryURL;
    private Inventory inventory;

    /**
     * Constructor that accepts a URL of inventory as a CSV file
     * and loads inventory based on CSV FILE
     *
     * @param inventoryURL url of the csv file
     */
    public Store(String inventoryURL) {
        inventory = new Inventory();
        this.inventoryURL = inventoryURL;
        loadInventoryFromWeb(inventoryURL);
    }

    /**
     * Loads inventory of the store based on the csv file given as URL
     *
     * @param inventoryURL url of the csv file
     */
    public void loadInventoryFromWeb(String inventoryURL) {
        try {
            URL url = new URL(inventoryURL);
            String str = IOUtils.toString(url.openStream(), "UTF-8");
            String[] lines = str.split("\n");
            for (int i = 0; i < lines.length; i++) {
                System.out.println(lines[i]);
                String line = lines[i];
                String[] words = line.split(",");
                // here is what a lines looks like, there are multiple platforms and genres (separated by ; )
                // PC,StarCraft 2,06/27/2010, Blizzard, Strategy, 59.99, T, 100
                List<String> platform = new ArrayList<String>(Arrays.asList(words[0].split(";")));
                String title = words[1];
                String[] nums = words[2].split("/");
                LocalDate releaseDate = LocalDate.of(Integer.parseInt(nums[2]), Integer.parseInt(nums[0]),
                        Integer.parseInt(nums[1]));
                String publisher = words[3];
                List<String> genres = new ArrayList<String>(Arrays.asList(words[4].split(";")));
                double price = Double.parseDouble(words[5].trim());
                String rating = words[6].trim();
                int score = Integer.parseInt(words[7].trim());

                Game game = new Game(platform, title, releaseDate, publisher, genres, price, rating, score);
                inventory.add(game);
            }
        } catch (IOException e) {

        }

    }

    public Store() {
        inventory = new Inventory();
    }

    public Inventory getInventory() {
        return inventory;
    }
}
