package org.example.midterm;

public class Lawyer extends Employee {
    public Lawyer(String name) {
        super(name);
    }

    @Override
    public int getVacationDays() {
        return 1;
    }
}
