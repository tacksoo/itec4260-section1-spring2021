package org.example;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a inventory of Game objects
 * this class will be used to find interesting properties of the
 * games inventory such as least expensive game, average price, etc
 *
 */
public class Inventory {
    private List<Game> games = new ArrayList<>();

    public Inventory() {
    }




    /**
     * Returns the size of the inventory, 0 if empty inventory
     * @return the size of inventory, will always be non-negative
     */
    public int size() {
        return games.size();
    }


    /**
     * Adds a game object to the inventory. Duplicates are allowed
     *
     * @param game game object to be added, no check for null
     */
    public void add(Game game) {
        games.add(game);
    }

    /**
     * Removes given game object. Does not check for null.
     * If more than one copy of the games is added to the inventory, only the first
     * occurance of the game is deleted.
     * @param game Game to remove
     */
    public void remove(Game game) {
        games.remove(game);
    }

    /**
     * Returns the cheapest game in terms of price
     * @return cheapest game object
     * @throws IllegalStateException Can't find the cheapest when the inventory is empty
     */
    public Game getCheapestGame() throws IllegalStateException {
        if(this.size() == 0)
            throw new IllegalArgumentException("Sorry, the inventory is empty");
        Game cheapest = games.get(0);
        for (int i = 0; i < games.size(); i++) {
            if( cheapest.getPrice() > games.get(i).getPrice()) {
                cheapest = games.get(i);
            }
        }
        return cheapest;
    }

    /**
     * Returns the most highly rated game in the inventory. Most highly here means how good a game is to players and critics.
     * @return game object with highest rating score
     */
    public Game getMostHighlyRatedGame() {
        if(this.size() == 0)
            throw new IllegalArgumentException("Sorry, the inventory is empty");
        Game best = games.get(0);
        for (int i = 0; i < games.size(); i++) {
            if (best.getScore() < games.get(i).getScore()) {
                best = games.get(i);
            }
        }
        return best;
    }

    /**
     * Returns the average price of all games in inventory
     * @return the average price of all games
     */
    public double getAveragePriceOfAllGames() {
        double total = 0;
        for(Game g: games) {
            total += g.getPrice();
        }
        return total/games.size();
    }

    /**
     * Print to console the average price of all games
     */
    public void printAveragePriceOfAllGames() {
        System.out.println(getAveragePriceOfAllGames());
    }
}
