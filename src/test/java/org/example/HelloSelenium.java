package org.example;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HelloSelenium {

    public static WebDriver driver;

    @BeforeClass
    public static void setUpChrome() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testGoogle() {
        driver.get("https://www.google.com");
        WebElement queryBox = driver.findElement(By.name("q"));
        queryBox.sendKeys("GGC");
        queryBox.submit();
        Assert.assertTrue(driver.getTitle().contains("GGC"));
    }
}
