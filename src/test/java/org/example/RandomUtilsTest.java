package org.example;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.SystemUtils;
import org.junit.Test;

public class RandomUtilsTest {

    @Test
    public void testRandom() {
        int r = RandomUtils.nextInt(1,10);
        System.out.println(r);
        String x = RandomStringUtils.randomAlphabetic(10);
        System.out.println(x);
    }

    @Test
    public void testSystem() {
        System.out.println(SystemUtils.OS_NAME);
        System.out.println(SystemUtils.USER_LANGUAGE);
        System.out.println(SystemUtils.USER_NAME);
        System.out.println(SystemUtils.OS_ARCH);
    }
}
