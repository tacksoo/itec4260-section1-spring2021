package org.example;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class LinkedInTest {

    private static WebDriver driver;
    private static Connection connection;
    private final static String DB_URL = "jdbc:sqlite:people.db";

    @BeforeClass
    public static void setUp() throws SQLException {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        connection = DriverManager.getConnection(DB_URL);
    }

    @Test
    public void testPersonLinkedIn() throws IOException {
        List<String> lines = FileUtils.readLines(new File("students.txt"), "UTF-8");
        for (int i = 0; i < lines.size(); i++) {
            driver.get("https://www.google.com");
            WebElement box = driver.findElement(By.name("q"));
            box.sendKeys(lines.get(i) + " linkedin");
            box.submit();
            WebElement urlElement = driver.findElement(By.cssSelector("#rso > div:nth-child(1) > div:nth-child(1) > div > div.yuRUbf > a"));
            String url = urlElement.getAttribute("href");
            List<WebElement> headings = driver.findElements(By.tagName("h3"));
            String title = headings.get(0).getText();
            addPersonToDB(lines.get(i), url, title);
        }
    }

    // if the person has no linkedin presence, google returns incorrect data
    public void addPersonToDB(String name, String url, String title) {
        String sql = "insert into students (name, url, description) values (?, ?, ?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, url);
            ps.setString(3, title);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void cleanUp() throws SQLException {
        connection.close();
    }
}
