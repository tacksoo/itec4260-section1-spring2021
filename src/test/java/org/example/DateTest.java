package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

public class DateTest {

    @Test
    public void testDate() {
        LocalDate depart = LocalDate.of(2021, 4, 7);
        LocalDate arrive = depart.plusDays(6);
        Assert.assertEquals(13,arrive.getDayOfMonth());
        LocalDate a = LocalDate.of(2021, 2, 27);
        LocalDate b = a.plusDays(6);
        Assert.assertEquals(5,b.getDayOfMonth());
        Assert.assertEquals(3,b.getMonthValue());
    }

    @Test
    public void testTreeSet() {
        Set<String> names = new TreeSet<>();
        names.add("oleg");
        names.add("ezekiel");
        names.add("jacob");
        names.add("keyvan");
        names.add("taisann");
        names.add("oleg");
        Assert.assertEquals(5,names.size());
        System.out.println(names);
    }

    @Test
    public void testPriorityQueue() {
        PriorityQueue<String> names = new PriorityQueue<>();
        names.add("keyvan");
        names.add("taisann");
        names.add("jacob");
        names.add("ezekiel");
        names.add("oleg");
        Assert.assertEquals("ezekiel",names.poll());
        Assert.assertEquals("jacob",names.peek());
    }






}
