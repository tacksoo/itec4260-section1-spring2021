package org.example;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InventoryMockTest {

    @Test
    public void testCheapestMock() throws Exception {
        InventoryInterface inv = Mockito.mock(InventoryInterface.class);
        List<String> platform = new ArrayList<>();
        platform.add("Nintendo Switch");
        List<String> genres = new ArrayList<>();
        genres.add("Sports");
        Game nfl = new Game(platform,"Madden NFL", LocalDate.of(2021,1,1),"EA",
                genres,60,"E");
        // program the mock object "inv" to return the "nfl" object when findCheapestGame() is called
        Mockito.when(inv.findCheapestGame()).thenReturn(nfl);
        Assert.assertEquals("Madden NFL",inv.findCheapestGame().getName());
    }



}
