package org.example;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.*;

public class HelloDatabase {
    // jdbc:sqlite:cars.db
    private final static String connectionURL = "jdbc:sqlite:cars.db";
    private static Connection connection;

    @BeforeClass
    public static void connectDB() throws SQLException {
        connection = DriverManager.getConnection(connectionURL);
    }

    @Test
    public void testVehicles() throws SQLException {
        String sql = "select count(*) from vehicles;";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet result = ps.executeQuery();
        System.out.println(result.getInt("count(*)"));
    }


    @AfterClass
    public static void cleanUp() throws SQLException {
        connection.close();
    }
}
