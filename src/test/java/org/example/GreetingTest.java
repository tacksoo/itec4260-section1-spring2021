package org.example;

import org.example.midterm.Greeting;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class GreetingTest {

    public Greeting getHighestGreeting (List<Greeting> list) {
        if (list.size() == 0) return null;
        Greeting highest = list.get(0);
        int highestID = list.get(0).getId();
        for (int i = 0; i < list.size(); i++) {
            if( highestID < list.get(i).getId()) {
                highest = list.get(i);
                highestID = list.get(i).getId();
            }
        }
        return highest;
    }

    @Test
    public void testHighestGreeting() {
        // test if list is size 0
        List<Greeting> greetings = new ArrayList<>();
        Assert.assertEquals(null, getHighestGreeting(greetings));
        greetings.add(new Greeting(10, "hello"));
        greetings.add(new Greeting(200, "hi"));
        greetings.add(new Greeting(30, "yo"));
        greetings.add(new Greeting(7, "hey"));
        Assert.assertEquals(200, getHighestGreeting(greetings).getId());
    }

}
