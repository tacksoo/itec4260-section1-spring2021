package org.example;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class StonkTest {

    @Test
    public void testStonkCSV() throws IOException {
        URL gistURL = new URL("https://gist.githubusercontent.com/tacksoo/b9edbfc8c03e1ca89d459bf1af39842d/raw/75abf553a0297d9202b1a568f185f735055d6f81/stonks.csv");
        String str = IOUtils.toString(gistURL.openStream(),"UTF-8");
        String[] lines = str.split("\n");
        String lowestLine = "";
        double lowestValue = Double.MAX_VALUE;
        for (int i = 1; i < lines.length; i++) {
            String[] rowElements = lines[i].split(",");
            double value = Double.parseDouble(rowElements[2].trim().substring(0,rowElements[2].length()-2));
            if (value < lowestValue) {
                lowestValue = value;
                lowestLine = lines[i];
            }
        }
        FileUtils.writeStringToFile(new File("stonk.csv"),lowestLine,"UTF-8");
    }

    @Test
    public void testLineCount() throws IOException {
        URL gistURL = new URL("https://gist.githubusercontent.com/tacksoo/d1fcb51f8921cdc90d1ffadb0b63b768/raw/6c9a8b9ffadd87b4bd0217b91cdd90bb9e227ef2/schedule.csv");
        String str = IOUtils.toString(gistURL.openStream(),"UTF-8");
        Assert.assertEquals(29, str.split("\n").length);
    }

}
