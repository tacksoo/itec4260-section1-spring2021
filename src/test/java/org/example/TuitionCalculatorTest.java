package org.example;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TuitionCalculatorTest {

    private static WebDriver driver;
    private static final String USNEWS_URL = "https://www.usnews.com/best-colleges/georgia-gwinnett-college-41429";
    private static final String GGC_TUITION_CALC = "https://www.ggc.edu/admissions/tuition-and-financial-aid-calculators/index.html#";
    private static final String BANNER_URL = "https://ggc.gabest.usg.edu/pls/B400/twbkwbis.P_WWWLogin";

    @BeforeClass
    public static void setUpWebDriver() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testInStateTuition() {
       double instateCalc = getTuitionFromCalc("instate");
       double instateNews = getTuitionFromNews("instate");
       Assert.assertEquals(instateNews, instateCalc*2, 10);
    }

    public double getTuitionFromNews(String status) {
        driver.get(USNEWS_URL);
        String selector = status.equals("instate") ?
                "#app > div > div:nth-child(1) > div.Heading__ProfileHeadingBox-sc-6a32fq-5.kmKRSP > div > div > div > div.Villain__FlexDiv-sc-1y12ps5-0.duSOMV > div.Villain__SupplementColumn-sc-1y12ps5-2.bjOYsD > div > div > div > div.Panel__Content-ejd83f-0.hygZCJ.border-right.border-left.border-bottom > table > tbody > tr:nth-child(1) > div > a.Anchor-byh49a-0.esWvXN"
                : "#app > div > div:nth-child(1) > div.Heading__ProfileHeadingBox-sc-6a32fq-5.kmKRSP > div > div > div > div.Villain__FlexDiv-sc-1y12ps5-0.duSOMV > div.Villain__SupplementColumn-sc-1y12ps5-2.bjOYsD > div > div > div > div.Panel__Content-ejd83f-0.hygZCJ.border-right.border-left.border-bottom > table > tbody > tr:nth-child(2) > div > a.Anchor-byh49a-0.esWvXN";
        WebElement element = driver.findElement(By.cssSelector(selector));
        String tuitionString = element.getText();
        String tuition = tuitionString.split(" ")[0].replace("$","").replace(",","");
        return Double.parseDouble(tuition);
    }

    public double getTuitionFromCalc(String status) {
        driver.get(GGC_TUITION_CALC);
        String label = status.equals("instate") ?
                "#main-content > div > article > div > div:nth-child(4) > form > div:nth-child(1) > div > div > div:nth-child(1) > fieldset > div > label:nth-child(1)"
        : "#main-content > div > article > div > div:nth-child(4) > form > div:nth-child(1) > div > div > div:nth-child(1) > fieldset > div > label:nth-child(2)";
        WebElement inOrOutLabel = driver.findElement(By.cssSelector(label));
        inOrOutLabel.click();
        Select creditHours = new Select(driver.findElement(By.cssSelector("#creditHOURS")));
        creditHours.selectByValue("15");
        WebElement tuitionElement = driver.findElement(By.cssSelector("#totalcost"));
        String tuitionString = tuitionElement.getAttribute("value");
        double tuition = Double.parseDouble(tuitionString.replace("$",""));
        return tuition;
    }

    @AfterClass
    public static void cleanUp() throws Exception {
        Thread.sleep(7000);
        driver.close();
    }
}
