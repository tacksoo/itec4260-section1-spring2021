package org.example;

import org.example.midterm.Lawyer;
import org.junit.Assert;
import org.junit.Test;

public class LawyerTest {

    @Test
    public void testLawyerVacation() {
        Lawyer rudy = new Lawyer("Guiliani");
        Assert.assertEquals(1, rudy.getVacationDays());
    }
}
