package org.example;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class InventoryTest {

    @Test
    public void testEmptyInventory() {
        Inventory games = new Inventory();
        int size = games.size();
    }

    @Test
    public void testAddInventory() {
        Inventory games = new Inventory();
        games.add(new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "PS1")),
                "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", new ArrayList<>(Arrays.asList("Action")), 59.99, "M"));
        Assert.assertEquals(1, games.size());
        games.add(new Game(new ArrayList<>(Arrays.asList("PC", "Mac")),
                "Overwatch", LocalDate.of(2016, 1, 1),
                "Blizzard", new ArrayList<>(Arrays.asList("3d")), 59.99, "M"));
        Assert.assertEquals(2, games.size());
    }

    @Test
    public void testRemoveInventory() {
        Inventory games = new Inventory();
        Game diablo = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "PS1")),
                "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", new ArrayList<>(Arrays.asList("Action")), 59.99, "M");
        games.add(diablo);
        Game overwatch = new Game(new ArrayList<>(Arrays.asList("PC", "Mac")),
                "Overwatch", LocalDate.of(2016, 1, 1),
                "Blizzard", new ArrayList<>(Arrays.asList("3d")), 59.99, "M");
        games.add(overwatch);
        games.remove(diablo);
        Assert.assertEquals(1, games.size());
        games.remove(overwatch);
        Assert.assertEquals(0, games.size());
    }

    @Test
    public void testGetCheapest() {
        Inventory games = new Inventory();
        Game diablo = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "PS1")),
                "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", new ArrayList<>(Arrays.asList("Action")), 59.99, "M");
        games.add(diablo);
        Game overwatch = new Game(new ArrayList<>(Arrays.asList("PC", "Mac")),
                "Overwatch", LocalDate.of(2016, 1, 1),
                "Blizzard", new ArrayList<>(Arrays.asList("3d")), 49.99, "M");
        games.add(overwatch);
        Assert.assertEquals(overwatch, games.getCheapestGame());
    }

    @Test
    public void testMostHighlyRated() {
        Inventory games = new Inventory();
        Game diablo = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "PS1")),
                "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", new ArrayList<>(Arrays.asList("Action")), 59.99, "M", 99);
        games.add(diablo);
        Game overwatch = new Game(new ArrayList<>(Arrays.asList("PC", "Mac")),
                "Overwatch", LocalDate.of(2016, 1, 1),
                "Blizzard", new ArrayList<>(Arrays.asList("3d")), 49.99, "M", 100);
        games.add(overwatch);
        Game g = games.getMostHighlyRatedGame();
        Assert.assertTrue(g.equals(overwatch));
    }

    @Test
    public void testAveragePrice() {
        Inventory games = new Inventory();
        Game diablo = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "PS1")),
                "Diablo", LocalDate.of(1997, 1, 3),
                "Blizzard North", new ArrayList<>(Arrays.asList("Action")), 50, "M", 99);
        games.add(diablo);
        Game overwatch = new Game(new ArrayList<>(Arrays.asList("PC", "Mac")),
                "Overwatch", LocalDate.of(2016, 1, 1),
                "Blizzard", new ArrayList<>(Arrays.asList("3d")), 40, "M", 100);
        games.add(overwatch);
        double averagePrice = games.getAveragePriceOfAllGames();
        Assert.assertEquals(45, averagePrice, 0.01);
    }

}
