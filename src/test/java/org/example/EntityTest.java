package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

public class EntityTest {

    @Test
    public void testEntityName() {
        Assert.assertEquals("dr.im", new Entity("dr.im").getName());
    }

    @Test
    public void testFloats() {
        //Assert.assertTrue( 0.1 + 0.2 == 0.3);
        BigDecimal candy = new BigDecimal("0.3");
        BigDecimal price = candy.multiply( new BigDecimal("3.0"));
        System.out.println(price);
    }

    @Test
    public void testDate() {
        System.out.println(new Date());
    }
}
